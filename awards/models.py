from django.db import models

# Create your models here.
class Award(models.Model):
  CATEGORY_CHOICES = (
    ('jss1', 'JSS1'),    
    ('jss2', 'JSS2'),   
    ('jss3', 'JSS3'),  
    ('ss1', 'SS1'),
    ('ss2', 'SS2'),
    ('ss3', 'SS3'),
    ('teachers', 'Teachers'),
    ('staff', 'Staff')
  )
  award = models.CharField(max_length=30)
  name = models.CharField(max_length=30)
  category = models.CharField(max_length=30, choices=CATEGORY_CHOICES)
  image = models.ImageField(upload_to='award_pics', default='01.jpg')