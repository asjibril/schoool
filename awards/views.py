from django.shortcuts import render
from .models import Award

# Create your views here.
def awards(request):
  awards1 = Award.objects.filter(category='jss1')
  awards2 = Award.objects.filter(category='jss2')
  awards3 = Award.objects.filter(category='jss3')
  awards4 = Award.objects.filter(category='ss1')
  awards5 = Award.objects.filter(category='ss2')
  awards6 = Award.objects.filter(category='ss3')
  awards7 = Award.objects.filter(category='teachers')
  awards8 = Award.objects.filter(category='staff')

  return render(request, 'awards/awards.html', {
    'awards1': awards1,
    'awards2': awards2,
    'awards3': awards3,
    'awards4': awards4,
    'awards5': awards5,
    'awards6': awards6,
    'awards7': awards7,
    'awards8': awards8,
  })
