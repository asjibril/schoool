from django.urls import path
from . import views
from blog import views as blog_views

app_name = 'home'
urlpatterns = [
  path('', views.home, name='home'),
  path('blog/<int:pk>', blog_views.detail_post, name='detail_post'),
]