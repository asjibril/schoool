from django.shortcuts import render
from blog.models import BlogPost

# Create your views here.
def home(request):
  posts = BlogPost.objects.filter(active=True)[:4]

  return render(request, 'home/home.html', {'posts':posts})