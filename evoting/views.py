from django.shortcuts import render, get_object_or_404, redirect
from .models import Poll, Option
from django.http import HttpResponse, Http404

# Create your views here.
def poll_view(request, pk):
  poll = get_object_or_404(Poll, pk=pk)
  options = poll.options.all()

  if request.method == 'POST':
    #selected_option = poll.options.get(pk=request.POST['options'])
    selected_option = get_object_or_404(Option, pk=request.POST['options'])
    selected_option.votes += 1
    selected_option.save()
    return redirect('evoting:pollview', pk=poll.pk)
  else:
    return render(request, 'evoting/poll.html', {'poll':poll, 'options':options})

def poll_list(request):
  polls = Poll.objects.all()
  return render(request, 'evoting/list.html', {'polls': polls})

def polls_all(request):
  #polls = Poll.objects.all()
  poll1 = get_object_or_404(Poll, pk=1)
  poll2 = get_object_or_404(Poll, pk=2)
  #options = poll.options.all()

  if request.method == 'POST':
    #selected_option = poll.options.get(pk=request.POST['options'])
    first_poll_option = get_object_or_404(Option, pk=request.POST['poll1'])
    first_poll_option.votes += 1
    first_poll_option.poll.votes += 1
    first_poll_option.save()

    second_poll_option = get_object_or_404(Option, pk=request.POST['poll2'])
    second_poll_option.votes += 1
    second_poll_option.poll.votes += 1
    second_poll_option.save()
    return redirect('evoting:polls_all')
  

  return render(request, 'evoting/polls.html', {'poll1':poll1, 'poll2':poll2})