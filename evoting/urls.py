from django.urls import path
from . import views

app_name = 'evoting'
urlpatterns = [
  path('poll/', views.poll_list, name='polllist'),
  path('poll/<int:pk>/', views.poll_view, name='pollview'),
  path('polls/', views.polls_all, name='polls_all')
]