from django.urls import path, include
from . import views

app_name = 'anonymous'
urlpatterns = [
  path('report/', views.submit_report, name='report'),
  path('reports/', views.list_report, name='reports')
]
