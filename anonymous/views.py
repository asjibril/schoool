from django.shortcuts import render
from django.contrib import messages
from .forms import ReportForm
from .models import AnonReport

# Create your views here.
def submit_report(request):
  if request.method == 'POST':
    report_form = ReportForm(request.POST)
    if report_form.is_valid():
      new_report = report_form.save()
      messages.success(request, 'Report Submitted Successfully')
    else:
      messages.error(request, 'Failed to submit report')
    report_form = ReportForm()
  else:
    report_form = ReportForm()
    
  return render(request, 'anonymous/report.html', {'report_form': report_form})

def list_report(request):
  reports = AnonReport.objects.all().order_by('-reported')
  return render(request, 'anonymous/reports.html', {'reports': reports})
