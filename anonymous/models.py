from django.db import models
from django.utils import timezone

# Create your models here.
class AnonReport(models.Model):
  title = models.CharField(max_length=30)
  text = models.TextField()
  active = models.BooleanField(default=True)

  reported = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.title