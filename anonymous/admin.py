from django.contrib import admin
from .models import AnonReport

# Register your models here.
@admin.register(AnonReport)
class AnonReportAdmin(admin.ModelAdmin):
  list_display = ('title', 'text', 'active')