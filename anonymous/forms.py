from django import forms
from .models import AnonReport

class ReportForm(forms.ModelForm):
  class Meta:
    model = AnonReport
    fields = ('title', 'text')