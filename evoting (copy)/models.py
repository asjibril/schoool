from django.db import models

# Create your models here.
class Poll(models.Model):
  poll_text = models.CharField(max_length=60)
  votes = models.IntegerField(default=0)

  def __str__(self):
    return self.poll_text

class Option(models.Model):
  poll = models.ForeignKey(Poll, on_delete=models.CASCADE, related_name='options')
  option_text = models.CharField(max_length=30)
  votes = models.IntegerField(default=0)

  def __str__(self):
    return self.option_text