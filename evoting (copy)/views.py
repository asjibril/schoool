from django.shortcuts import render, get_object_or_404, redirect
from .models import Poll, Option
from django.http import HttpResponse, Http404

# Create your views here.
def poll_view(request, pk):
  poll = get_object_or_404(Poll, pk=pk)
  options = poll.options.all()

  if request.method == 'POST':
    #selected_option = poll.options.get(pk=request.POST['options'])
    selected_option = get_object_or_404(Option, pk=request.POST['options'])
    selected_option.votes += 1
    selected_option.save()
    return redirect('evoting:pollview', pk=poll.pk)
  else:
    return render(request, 'evoting/poll.html', {'poll':poll, 'options':options})

def poll_list(request):
  polls = Poll.objects.all()
  return render(request, 'evoting/list.html', {'polls': polls})