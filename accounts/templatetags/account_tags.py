from django import template
#from ../../ blog/models import BlogPost

register = template.Library()

@register.simple_tag
def draft_posts():
  return BlogPost.objects.filter(status='draft')

@register.simple_tag
def published_posts():
  return BlogPost.objects.filter(status='published')