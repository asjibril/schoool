from django.shortcuts import render
from django.http import HttpResponse
from .forms import LoginForm, RegistrationForm, ProfileEditForm
from .models import Profile
from blog.models import BlogPost

from django.contrib.auth import authenticate, login
# Create your views here.
def user_login(request):
  if request.method == 'POST':
    login_form = LoginForm(request.POST)
    if login_form.is_valid():
      cd = login_form.cleaned_data
      user = authenticate(request, username=cd['username'], password=cd['password'])
      if user is not None:
        if user.is_active:
          login(request, user)
          draft_posts = BlogPost.objects.filter(author=user).filter(status='draft')
          published_posts = BlogPost.objects.filter(author=user).filter(status='published')
          return render(request, 'accounts/dashboard.html', {'user':user, 'draft_posts':draft_posts, 'published_posts':published_posts})
        else: 
          return HttpResponse('Account Disabled')
      else: 
        return HttpResponse('Invalid Login')
  else:
    login_form = LoginForm()
    return render(request, 'accounts/login.html', {'login_form':login_form})

def register(request):
  if request.method == 'POST':
    register_form = RegistrationForm(request.POST)
    if register_form.is_valid():
      new_user = register_form.save(commit=False)
      new_user.set_password(register_form.cleaned_data['password'])
      new_user.save()
      Profile.objects.create(user=new_user)
      #return HttpResponse('Registration Successful')
      return render(request, 'accounts/dashboard.html', {'user': new_user})  
  else:
    register_form = RegistrationForm()
    return render(request, 'accounts/register.html', {'register_form':register_form})

def dashboard(request):
  user = request.user
  draft_posts = BlogPost.objects.filter(author=user).filter(status='draft')
  published_posts = BlogPost.objects.filter(author=user).filter(status='published')
  return render(request, 'accounts/dashboard.html', {'user':user, 'draft_posts':draft_posts, 'published_posts':published_posts})