from django import forms
from django.contrib.auth.models import User
from .models import Profile

# Code starts here
class LoginForm(forms.Form):
  username = forms.CharField()
  password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(forms.ModelForm):
  password = forms.CharField(label='Password', widget=forms.PasswordInput)
  password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)
  class Meta:
    model = User
    fields = ('username', 'first_name', 'email')

    def clean_password2(self):
      cd = self.cleaned_data
      if cd['password'] != ['password2']:
        raise forms.VAlidationError('Passwords dont match')
      return cd['password2']

class ProfileEditForm(forms.ModelForm):
  class Meta:
    model = Profile
    fields = ('photo', 'nicky', 'level', 'hobbies')