from django.db import models
from django.conf import settings

# Create your models here.
class Profile(models.Model):
  LEVEL_CHOICES = (
    ('jss1', 'JSS1'),
    ('jss2', 'JSS2'),
    ('jss3', 'JSS3'),
    ('ss1', 'SS1'),
    ('ss2', 'SS2'),
    ('ss3', 'SS3'),
  )
  user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  level = models.CharField(choices=LEVEL_CHOICES, max_length=22)
  nicky = models.CharField(max_length=30)
  photo = models.ImageField(upload_to='users/%Y/%m/%d/')
  hobbies = models.CharField(max_length=50, default='null')
  bio = models.TextField(default='Awesome')