from django.urls import path
from . import views
app_name = 'counselor'
urlpatterns = [
  path('', views.users_view, name='friends'),
  path('<slug:receiver>/', views.chat_view, name='chat'),
  path('post/<slug:receiver>/', views.post, name='post')
]