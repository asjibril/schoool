from django.shortcuts import render
from .models import Chat
from .forms import ChatForm
from django.contrib.auth.models import User

# Create your views here.
def chat_view(request, receiver):
  sender = request.user
  if request.method == 'POST':
    chat_form = ChatForm(request.POST)
    if chat_form.is_valid():
      new_chat = chat_form.save(commit=False)
      new_chat.sender = sender
      new_chat.to = receiver
      new_chat.save()

  chat_form = ChatForm()
  chat1 = Chat.objects.filter(sender=sender).filter(to=receiver).order_by('sent')[:10]
  chat2 = Chat.objects.filter(sender=receiver).filter(to=sender).order_by('sent')[:10]
  chats_merge = chat1 | chat2
  chats = chats_merge.distinct().order_by('sent')

  return render(request, 'counselor/chat.html', {'chat_form':chat_form, 'chats':chats, 'receiver':receiver, 'sender':sender})

def post(request, receiver):
  sender = request.user
  if request.method == 'POST':
    chat_form = ChatForm(request.POST)
    if chat_form.is_valid():
      new_chat = chat_form.save(commit=False)
      new_chat.sender = sender
      new_chat.to = receiver
      new_chat.save()

  chat_form = ChatForm()
  chat1 = Chat.objects.filter(sender=sender).filter(to=receiver).order_by('sent')[:10]
  chat2 = Chat.objects.filter(sender=receiver).filter(to=sender).order_by('sent')[:10]
  chats_merge = chat1 | chat2
  chats = chats_merge.distinct().order_by('sent')
  chatss = chats.count()

  return render(request, 'counselor/post.html', {'chat_form':chat_form, 'chats':chats, 'receiver':receiver, 'chatss':chatss})

def users_view(request):
  users = User.objects.all()
  return render(request, 'counselor/friends.html', {'users':users})
