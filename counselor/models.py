from django.db import models
from django.conf import settings
from django.utils import timezone
# Create your models here.
class Chat(models.Model): 
  sender = models.CharField(max_length=40)
  message = models.TextField()
  sent = models.DateTimeField(default=timezone.now)
  to = models.CharField(max_length=50)

  def __str__(self):
    return self.message