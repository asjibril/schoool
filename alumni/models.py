from django.db import models
from django.utils import timezone

# Create your models here.
class Alumni(models.Model):
  PROFESSION_FIELD_CHOICES = (
    ('medical', 'Medical Professional'),
    ('law', 'law'),
    ('it', 'IT')
  )
  name = models.CharField(max_length=30)
  pro_field = models.CharField(choices=PROFESSION_FIELD_CHOICES, max_length=30)
  image = models.ImageField(upload_to='alumni_pics', default='01.jpg')
  contact_email = models.EmailField()
  grad_year = models.IntegerField(default=0)
  profession = models.CharField(max_length=30)
  hobbies = models.CharField(max_length=300)
  motivation = models.CharField(max_length=300)
  success_secret = models.TextField()
  advice = models.TextField()
  story = models.TextField()
  created = models.DateTimeField(default=timezone.now)
  active = models.BooleanField(default=False)

  def __str__(self):
    return self.name