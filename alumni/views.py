from django.shortcuts import render
from .models import Alumni
from .forms import AlumniForm
# Create your views here.
def alumni_list(request):
  alumnis = Alumni.objects.filter(active=True)
  return render(request, 'alumni/list.html', {'alumnis': alumnis})

def alumni_create(request):
  if request.method == 'POST':
    form = AlumniForm(request.POST, request.FILES)
    if form.is_valid():
      new_alumni = form.save()
      return render(request, 'alumni/detail.html', {"alumni": new_alumni})
  else:
    form = AlumniForm()
    return render(request, 'alumni/createalumni.html', {'form':form})
  