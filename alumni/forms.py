from django import forms
from .models import Alumni
class AlumniForm(forms.ModelForm):
  class Meta:
    model = Alumni
    fields = ('name', 'image', 'grad_year', 'pro_field', 'profession', 'contact_email', 'hobbies', 'motivation', 'success_secret', 'advice', 'story')