from django.urls import path
from . import views


app_name = 'blog'
urlpatterns = [
  path('', views.list_post, name='list_post'),
  path('createpost/', views.create_post, name='post_new'),
  path('<int:pk>/editpost/', views.edit_post, name='editpost'),
  path('<int:pk>/', views.detail_post, name='detail_post'),
]