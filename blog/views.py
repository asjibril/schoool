from django.shortcuts import render, get_object_or_404, redirect
from .models import BlogPost, Comment
from .forms import PostForm, CommentForm
from django.contrib import messages

# Create your views here.
def detail_post(request, pk):
 post = get_object_or_404(BlogPost, pk=pk)
 if request.method == 'POST':
   comment_form = CommentForm(request.POST)
   if comment_form.is_valid():
     new_comment = comment_form.save(commit=False)
     new_comment.post = post
     new_comment.save()
 
 comment_form = CommentForm()
 comments = post.comments.filter(active=True)


 return render(request, 'blog/detail.html', {'post':post, 'comment_form':comment_form, 'comments':comments})

def edit_post(request, pk):
  post = get_object_or_404(BlogPost, pk=pk)
  if request.method == 'POST':
    post_form = PostForm(data=request.POST, files=request.FILES, instance=post)
    if post_form.is_valid():
      updated_post = post_form.save(commit=False)
      updated_post.author = request.user
      updated_post.save()
      messages.success(request, 'Post Updated Successfully')
      return redirect('blog:detail_post', pk=updated_post.pk)
  else:
    post_form = PostForm(instance=post)
    return render(request, 'blog/edit_post.html', {'post_form': post_form})
  
def create_post(request):
  if request.method == 'POST':
    post_form = PostForm(data=request.POST, files=request.FILES)
    if post_form.is_valid():
      new_post = post_form.save(commit=False)
      new_post.author = request.user
      new_post.save()
      messages.success(request, 'Post Submitted Successfully')
      return redirect('blog:detail_post', pk=new_post.pk)
  else:
    post_form = PostForm()
    return render(request, 'blog/create_post.html', {'post_form': post_form})

def list_post(request):
  posts = BlogPost.objects.filter(active=True)
  return render(request, 'blog/list.html', {'posts':posts})