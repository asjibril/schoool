from django.contrib import admin
from .models import Comment, BlogPost
# Register your models here.
@admin.register(BlogPost)
class BlogPostAdmin(admin.ModelAdmin):
  list_display = ('title', 'created', 'genre', 'active')
  list_editable = ('active',)

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
  list_display = ('name', 'text')