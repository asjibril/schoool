from django import forms
from .models import BlogPost, Comment

class PostForm(forms.ModelForm):
  class Meta:
    model = BlogPost
    fields = ('title', 'text', 'image', 'genre')

class CommentForm(forms.ModelForm):
  class Meta:
    model = Comment
    fields = ('name', 'text')