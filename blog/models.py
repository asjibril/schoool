from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.
class BlogPost(models.Model):
  STATUS_CHOICES = (
    ('draft', 'Draft'),
    ('published', 'Published')
  )
  GENRE_CHOICES = (
    ('health', 'Health'),
    ('education', 'Education'),
    ('tech', 'Tech'),
    ('entertainment', 'Entertainment'),
    ('others', 'Others')
  )
  author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  title = models.CharField(max_length=40)
  text = models.TextField()
  image = models.ImageField(upload_to='blog_pics', default='blog_pics/01.jpg')
  status = models.CharField(choices=STATUS_CHOICES, max_length=30, default='draft')
  created = models.DateTimeField(default=timezone.now)
  published = models.DateTimeField(blank=True, null=True)
  genre = models.CharField(choices=GENRE_CHOICES, max_length=30)
  active = models.BooleanField(default=False)

  def publish(self):
    self.published = timezone.now

  def __str__(self):
    return self.title

class Comment(models.Model):
  post = models.ForeignKey(BlogPost, on_delete=models.CASCADE, related_name='comments')
  name = models.CharField(max_length=30)
  text = models.TextField()
  active = models.BooleanField(default=True)
  created = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.text